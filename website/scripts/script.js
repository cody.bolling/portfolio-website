var pageChangeSpeed = 500;
var underlineChangeSpeed = 250;
var pageChanging = false;
var projectsGridHTML;

getProjectsData().then(function(result) {
  projectsGridHTML = result;
}, function(error) {
  console.log(error);
});

/*
  Main jQuery function
*/
$(document).ready(function($)
{
  /*
    Functions for when the mouse hovers and unhovers from main buttons

    Shows the underline if the mouse hovers over the given main button and keeps the unerline
    showing if the given main button is selected. If not selected, the underline will hide upon
    the mouse unhovering from the given main button.
  */
  $(".main-button").hover(function()
  {
    $(this).mainButtonUnderlineShow();
  }, function()
  {
    $(this).mainButtonUnderlineHide();
  });

  /*
    Function for when a main button is clicked

    First, sets the properties for the main buttons including the "page-selected" attribute and whether or not the
    button should have its underline shown or hidden. Then if at the bottom of the page, moves and shrinks the 
    buttons and the page title to the top of the page.
  */
  $(".main-button").click(function()
  {
    if (pageChanging == false && $(this).attr("page-selected") == "false")
    {
      pageChanging = true;
      $("#page-title").attr("page-selected", "false");
      $(this).setMainButtonProperties();
      moveMainButtonsToTop($);
      movePageTitleToTop($);

      if ($(this).attr("id") == "programming-projects-button")
      {
        showProjectCards($);
      }
      else
      {
        hideProjectCards($);
      }
    }
  });

  /*
    Function for when the page title is clicked

    If the page is not on the landing page it resets the main buttons to their default state including setting the
    "page-selected" attribute to false and moving them to the bottom of the page. Moves and grows the page title
    to the middle of page.
  */
  $("#page-title").click(function()
  {
    if (pageChanging == false && $(this).attr("page-selected") == "false")
    {
      pageChanging = true;
      $("#page-title").attr("page-selected", "true");
      $(".main-button").attr("page-selected", "false");
      $(".main-button").mainButtonUnderlineHide();
      moveMainButtonsToBottom($);
      movePageTitleToMiddle($);
      hideProjectCards($);
    }
  });
});

/*
  Pulls all data from the git-repos table using the AWS document client for dyanmoDB and creates an HTML snippet of the grid of project cards.
*/
async function getProjectsData()
{
  /*
    The credentials used are for an IAM User/Service Account that only has access to GetItem and Scan functions for the git-repos table alone
    which is why I'm not afraid to put them in the source code. I plan on finding a better way to authorize AWS access in the future.
  */
  AWS.config.update({
    region: "us-east-1",
    endpoint: 'https://dynamodb.us-east-1.amazonaws.com',
    accessKeyId: "AKIASDV4VTJFECXNEC65",
    secretAccessKey: "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"
  });

  var docClient = new AWS.DynamoDB.DocumentClient();
  var params = {
    TableName: "git-repos",
    ProjectionExpression: "slug, title, #url, #date, version, tags",
    ExpressionAttributeNames: {
      "#url": "url",
      "#date": "date"
    }
  };

  const scanResults = [];
  var items;
  do{
    items = await docClient.scan(params).promise();
    items.Items.forEach((item) => scanResults.push(item));
    params.ExclusiveStartKey = items.LastEvaluatedKey;
  }while(typeof items.LastEvaluatedKey !== "undefined");

  return getProjectsGridHTML(scanResults);
}

/*
  Given the repos data from the git-repos DynamoDb table, this will make all the project cards together into an HTML snippet.
*/
function getProjectsGridHTML(reposData)
{
  var html = "<div id=\"project-cards-grid\">";
  reposData.forEach((repo) => html += getProjectCardHTML(repo));
  html += "</div>";

  return html;
}

/*
  Given the data for a single repo, this will produce the HTML for a single project card.
*/
function getProjectCardHTML(repo)
{
  var date = repo.date.split("T")[0].split("-");
  var html = "<a href=\"" + repo.url + "\" target=\"_blank\" class=\"project-card-container\">" +
    "<div class=\"project-card\">" +
    "<h1 id=\"title\">" + repo.title + "</h1>" +
    "<h2 id=\"version\">v" + repo.version + " | " + date[1] + "-" + date[2] + "-" + date[0] + "</h2>" +
    "<ul id=\"tags-list\">";

  repo.tags.split(",").forEach(tag => html += "<li id=\"tag\">" + tag + "</li>");
  html += "</ul></div></a>";

  return html;
}

/*
  Adds the project cards grid html snippet to the page and fades it in.
*/
function showProjectCards($)
{
  $("body").append(projectsGridHTML);

  $("#project-cards-grid").stop(true, false).animate(
  {
    "opacity": "100%"
  }, pageChangeSpeed);
}

/*
  Fades out the project cards grid and removes the html from the page.
*/
function hideProjectCards($)
{
  $("#project-cards-grid").stop(true, false).animate(
  {
    "opacity": "0%"
  }, pageChangeSpeed / 2, function()
  {
    document.getElementById("project-cards-grid").remove();
  });
}

/*
  Shows the underline for the given main button.
*/
(function($){$.fn.mainButtonUnderlineShow = function() {
  $(this).children(".main-button-underline").stop(true, false).animate(
  {
    "opacity": "100%"
  }, underlineChangeSpeed);
};})(jQuery);

/*
  Hides the unerline for the given main button EXCEPT when the given main button is selected.
*/
(function($){$.fn.mainButtonUnderlineHide = function() {
  if ($(this).attr("page-selected") == "false")
  {
    $(this).children(".main-button-underline").stop(true, false).animate(
    {
      "opacity": "0%"
    }, underlineChangeSpeed);
  }
};})(jQuery);

/*
  Sets the properties for the main buttons when one is clicked. The given main button will have its "page-selected" attribute
  set to true and its underline will be shown. The other main button(s) will have "page-selected" set to false and the underline
  will be hidden.
*/
(function($){$.fn.setMainButtonProperties = function() {
  $(this).attr("page-selected", "true");
  $(this).mainButtonUnderlineShow();

  for (var i = 0; i < $(".main-button").length; i++)
  {
    if ($(this).attr("id") != $($(".main-button")[i]).attr("id"))
    {
      $($(".main-button")[i]).attr("page-selected", "false");
      $($(".main-button")[i]).mainButtonUnderlineHide();
    }
  }
};})(jQuery);

/*
  Moves the main buttons to the top of the page and shrinks them.
*/
function moveMainButtonsToTop($)
{
  $(".main-button").stop(true, false).animate(
  {
    "top": "3vh",
    "height": "15vh",
    "width": "20vw"
  }, pageChangeSpeed, function()
  {
      pageChanging = false;
  });
}

/*
  Moves the main buttons to the bottom of the page and grows them.
*/
function moveMainButtonsToBottom($)
{
  $(".main-button").stop(true, false).animate(
  {
    "top": "72vh",
    "height": "25vh",
    "width": "30vw"
  }, pageChangeSpeed, function()
  {
      pageChanging = false;
  });
}

/*
  Move the page title to the top of the page and shrinks it.
*/
function movePageTitleToTop($)
{
  $("#page-title-container").stop(true, false).animate(
  {
    "top": "6vh"
  }, pageChangeSpeed);

  $("#page-title").stop(true, false).animate(
  {
    "font-size": "2vw"
  }, pageChangeSpeed);

  $("#short-bio").stop(true, false).animate(
  {
    "opacity": "0%"
  }, pageChangeSpeed / 2);
}

/*
  Move the page title to the middle of the page and grows it.
*/
function movePageTitleToMiddle($)
{
  $("#page-title-container").stop(true, false).animate(
  {
    "top": "30vh"
  }, pageChangeSpeed);

  $("#page-title").stop(true, false).animate(
  {
    "font-size": "4vw"
  }, pageChangeSpeed);

  $("#short-bio").stop(true, false).animate(
  {
    "opacity": "100%"
  }, pageChangeSpeed);
}
